var customers = [];

function findCustomer (Id) {
  return customers[findCustomerKey(Id)];
}

function findCustomerKey (Id) {
  for (var key = 0; key < customers.length; key++) {
    if (customers[key].id == Id) {
      return key;
    }
  }
}

var customerService = {
  findAll(fn) {
    axios.get('/api/v1/customers')
        .then(response => fn(response))
        .catch(error => console.log(error))
  },

  findById(id, fn) {
    axios
      .get('/api/v1/customers/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  create(customer, fn) {
    axios
      .post('/api/v1/customers', customer)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  update(id, customer, fn) {
    axios
      .put('/api/v1/customers/' + id, customer)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  deleteCustomer(id, fn) {
    axios
      .delete('/api/v1/customers/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
  }
};

var List = Vue.extend({
  template: '#customer-list',
  data: function () {
    return {customers: [], searchKey: ''};
  },
  computed: {
    filteredCustomers() {
      return this.customers.filter((customer) => {
      	return customer.firstName.indexOf(this.searchKey) > -1
      	  || customer.lastName.indexOf(this.searchKey) > -1
      	  || customer.email.toString().indexOf(this.searchKey) > -1
      })
    }
  },
  mounted() {
    customerService.findAll(r => {this.customers = r.data; customers = r.data})
  }
});

var Customer = Vue.extend({
  template: '#customer',
  data: function () {
    return {customer: findCustomer(this.$route.params.customer_id)};
  }
});

var EditCustomer = Vue.extend({
  template: '#customer-edit',
  data: function () {
    return {customer: findCustomer(this.$route.params.customer_id)};
  },
  methods: {
    updateCustomer: function () {
      customerService.update(this.customer.id, this.customer, r => router.push('/'))
    }
  }
});

var DeleteCustomer = Vue.extend({
  template: '#customer-delete',
  data: function () {
    return {customer: findCustomer(this.$route.params.customer_id)};
  },
  methods: {
    deleteCustomer: function () {
      customerService.deleteCustomer(this.customer.id, r => router.push('/'))
    }
  }
});

var AddCustomer = Vue.extend({
  template: '#add-customer',
  data() {
    return {
      customer: {firstName: '', lastName: '', email: ''}
    }
  },
  methods: {
    createCustomer() {
      customerService.create(this.customer, r => router.push('/'))
    }
  }
});

var router = new VueRouter({
	routes: [
		{path: '/', component: List},
		{path: '/customer/:customer_id', component: Customer, name: 'customer'},
		{path: '/add-customer', component: AddCustomer},
		{path: '/customer/:customer_id/edit', component: EditCustomer, name: 'customer-edit'},
		{path: '/customer/:customer_id/delete', component: DeleteCustomer, name: 'customer-delete'}
	]
});

new Vue({
  router
}).$mount('#app')
