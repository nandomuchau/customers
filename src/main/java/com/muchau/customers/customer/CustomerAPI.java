package com.muchau.customers.customer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/customers")
@Slf4j
@RequiredArgsConstructor
public class CustomerAPI {

    private final CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<Customer>> findAll() {
        return ResponseEntity.ok(customerService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.save(customer));
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Long id) {
        Optional<Customer> customer = customerService.findById(id);

        if (!customer.isPresent()) {
            return ResponseEntity.badRequest().body("Customer Id " + id + " does not exist!");
        }

        return ResponseEntity.ok(customer.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable Long id, @Valid @RequestBody Customer customer) {

        if (!customerService.findById(id).isPresent()) {
            return ResponseEntity.badRequest().body("Customer Id " + id + " does not exist!");
        }

        return ResponseEntity.ok(customerService.save(customer));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        if (!customerService.findById(id).isPresent()) {
            return ResponseEntity.badRequest().body("Customer Id " + id + " does not exist!");
        }

        customerService.deleteById(id);

        return ResponseEntity.accepted().build();
    }
}
