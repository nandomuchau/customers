# Code challenge - Customers CRUD
I really enjoyed working on this challenge :)

Frontend CRUD: http://localhost:8080/
* NOTE: The frontend does not include all the fields. It is just a test using VIE.JS.  

Backend CRUD: Import the PostMan settings: ```Customers.postman_collection.json```
* NOTE: File is located in the project root path.

Sample UnitTests for create a list are implemented :)

### Build
* Start your Docker server
* Run: docker-composer up
* Stop/kill: Control + C

### Cheat sheet

* Stops containers and removes containers ```docker-compose down```
* Stop all containers: ```docker stop $(docker ps -a -q)```
* Delete all containers: ```docker rm $(docker ps -a -q)```                       


### Possible issues. 
If you have this error: "Host 'xxx.xx.xx.x' is not allowed to connect to this MySQL server"

DO THIS

1- From a terminal, connect you to your MySQL running container
```docker exec -it database bash```

2- In your container, run this command
```mysqld --verbose --help | grep bind-address```

It will display address to bind to, for example like this:
```bind-address 0.0.0.0``` or ```bind-address *```

The bind address have to be 0.0.0.0 or * (which means "every IP addresses") to work.

[Reference](https://github.com/docker-library/mysql/issues/275)
